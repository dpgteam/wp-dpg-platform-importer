<?php

/**
 * Fired during plugin activation
 *
 * @link       http://digitalpropertygroup.com
 * @since      1.0.0
 *
 * @package    Wp_DPG_Platform_Importer
 * @subpackage Wp_DPG_Platform_Importer/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Wp_DPG_Platform_Importer
 * @subpackage Wp_DPG_Platform_Importer/includes
 * @author     Paul Beynon <paul@digitalpropertygroup.com>
 */
class Wp_DPG_Platform_Importer_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
