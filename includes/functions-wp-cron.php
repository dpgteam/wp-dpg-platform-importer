<?php
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
/**
 * Defined custom intervals for cron schedules.
 * @param  array $schedules
 * @return array
 */
function dpg_custom_cron_schedules($schedules){
    if(!isset($schedules["1min"])){
        $schedules["1min"] = array(
            'interval' => 60,
            'display' => __('Once every minute'));
    }
    if(!isset($schedules["5min"])){
        $schedules["5min"] = array(
            'interval' => 5*60,
            'display' => __('Once every 5 minutes'));
    }
    if(!isset($schedules["30min"])){
        $schedules["30min"] = array(
            'interval' => 30*60,
            'display' => __('Once every 30 minutes'));
    }
    return $schedules;
}
add_filter( 'cron_schedules', 'dpg_custom_cron_schedules' );
/**
 * Schedules cron jobs for syncing with DPG Platform.
 * @return void
 */
// function schedule_cron_jobs() {
//     wp_schedule_event( time(), '1min', ['Wp_DPG_Platform_Importer', 'dpg_runPropertySync'] );
//     wp_schedule_event( time(), '5min', ['Wp_DPG_Platform_Importer', 'dpg_runPropertyForceSync'] );
// }
/**
 * Runs the sync job.
 * @return void
 */
function dpg_runPropertySync() {
    exec('wget --quiet -O /dev/null http://test-import.dev/api/dpg_platform_importer/sync/');
    file_put_contents(__DIR__ . '../logs/property_sync.log', 'Property sync at ' . time(), FILE_APPEND);
    // file_put_contents(__DIR__ . '/get_properties.txt', 'Synced ' . time());
}
add_action('dpg_runPropertySync', 'my_cronjob_action');
/**
 * Runs the force_sync job.
 * @return void
 */
function dpg_runPropertyForceSync() {
    exec('wget --quiet -O /dev/null http://test-import.dev/api/dpg_platform_importer/force_sync/');
    file_put_contents(__DIR__ . '../logs/property_sync.log', 'Reset Property Sync at ' . time(), FILE_APPEND);
}
add_action('dpg_runPropertyForceSync', 'my_cronjob_action');
