<?php
/**
Controller name: DPG Platform Importer
Controller description: Synchronises agency data via the DPG Platform API.
 */
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

require(__DIR__ . '/traits/AgentImportTrait.php');
require(__DIR__ . '/traits/ControllerTrait.php');
require(__DIR__ . '/traits/NeighbourhoodImportTrait.php');
require(__DIR__ . '/traits/OfficeImportTrait.php');
require(__DIR__ . '/traits/PropertyImportTrait.php');
require(__DIR__ . '/traits/SuburbDataImportTrait.php');
/**
 * Fired during plugin deactivation
 *
 * @link       http://digitalpropertygroup.com
 * @since      1.0.0
 *
 * @package    Wp_DPG_Platform_Importer
 * @subpackage Wp_DPG_Platform_Importer/includes
 */

/**
 * Adds endpoint for synchronising agency data with the DPG Platform API.
 *
 * @since      1.0.0
 * @package    Wp_DPG_Platform_Importer
 * @subpackage Wp_DPG_Platform_Importer/includes
 * @author     Paul Beynon <paul@digitalpropertygroup.com>
 */
class JSON_API_Dpg_Platform_Importer_Controller {
	use AgentImportTrait;
	use ControllerTrait;
	use NeighbourhoodImportTrait;
    use OfficeImportTrait;
    use PropertyImportTrait;
	use SuburbDataImportTrait;
    /**
     * Synchronises local property data with DPG API.
     * @return json
     */
	public function sync() {
        $this->initController();
        try {
            $this->getProperties();
            /* Increment or reset the query offset value. */
            $new_offset = wp_count_posts('property')->publish  + 4000 < $this->offset ? 0 : intval($this->offset + $this->limit);
            update_option('property_sync_offset', $new_offset);

            return [
    			'offset'             => $this->offset,
    			'limit'              => $this->limit,
    			'properties_updated' => $this->properties_updated,
    			'properties_created' => $this->properties_created,
    			'agents_updated'     => count(array_unique($this->agents_updated)),
    			'agents_created'     => count(array_unique($this->agents_created)),
                'suburbs_created'    => $this->suburbs_created ?? 0,
    			'properties'    => $this->props,
            ];
        } catch (Exception $e) {
            return $this->exception($e);
        }
	}
	/**
     * Marks all property to be updated and resets the sync offset option.
     * @return json
     */
    public function force_sync()
    {
		$this->initController();
        try {
            global $wpdb;
            $id = $wpdb->update(
                'wp_postmeta',
				['meta_value' => 1],
				['meta_key'   => 'force_update']
            );
            update_option('property_sync_offset', 0);
            return [
				'code'    => 'OK',
				'message' => 'Sync Offset Reset',
            ];
        } catch (Exception $e) {
            return $this->exception($e);
        }
    }
    /**
     * Gets the agency's branches and creates drafts
     * for offices which don't exist locally.
     * @return json
     */
    public function sync_offices() {
        $this->initController();
        try {
            return $this->getOffices();
        } catch (Exception $e) {
            $this->exception($e);
        }
    }
}
