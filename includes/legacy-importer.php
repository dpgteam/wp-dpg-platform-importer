<?php
/**
Controller name: DPG Platform Legacy Importer
Controller description: Legacy DPG Platform Importer
 */

/**
 * Refer to for sold property example:
 * https://platform.digitalpropertygroup.com/api/v1/properties/23199?api_token=KoAViLFXjXR5CTnSCRMaJzn7caBplNe3yvfkRCrff3thKtpQQAwTRd68Y0cd
 */
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class JSON_API_Dpg_Properties_Controller
{
    private $offset;
    private $limit;
    private $properties_updated;
    private $properties_created;
    private $agents_created;
    private $agents_updated;
    private $suburbs_created;

    public function __construct()
    {
        $this->offset = get_option('property_sync_offset', 0);
        $this->limit = 100;
        $this->properties_updated = 0;
        $this->properties_created = 0;
        $this->agents_created = 0;
        $this->agents_updated = 0;
        $this->suburbs_created = 0;
    }

    /**
     * Marks all property to be updated
     *
     * @return array
     */
    public function force_sync()
    {
        global $wpdb;

        try {
            $id = $wpdb->update(
                'wp_postmeta',
                ['meta_value' => 1],
                ['meta_key' => 'force_update']
            );

            update_option('property_sync_offset', 0);

            return [];
        } catch (Exception $e) {
            return [
                'code' => $e->getCode(),
                'message' => $e->getMessage()
            ];
        }
    }

    public function get_suburbs_filtered()
    {
        global $wpdb;

        try {
            parse_str($_SERVER['QUERY_STRING'], $params);

            $property_type = 'AND metaPropertyType.meta_key = "property_type" AND metaPropertyType.meta_value = "' . $params['property_type'] . '"';
            $property_status = 'AND metaPropertyStatus.meta_key = "property_status" AND metaPropertyStatus.meta_value = "' . (!empty($params['property_status']) ? $params['property_status'] : 'current') . '"';
            $rental = '';
            $sales = '';
            $category = '';
            $land = false;
            $bedrooms = '';
            $bathrooms = '';
            $car_spaces = '';
            $price = '';
            $inspection = '';

            if ($params['property_type'] == 'residential_sale') {
                $property_type = 'AND metaPropertyType.meta_key = "property_type" AND metaPropertyType.meta_value IN ("residential", "land")';
            } elseif ($params['property_type'] == 'residential_rent') {
                $property_type = 'AND metaPropertyType.meta_key = "property_type" AND metaPropertyType.meta_value = "rental"';
            } elseif ($params['property_type'] == 'commercial_lease') {
                $property_type = 'AND metaPropertyType.meta_key = "property_type" AND metaPropertyType.meta_value = "commercial"';
                $rental = 'AND metaRental.meta_key = "rent_amount" AND metaRental.meta_value > 0';
            } elseif ($params['property_type'] == 'commercial_sale') {
                $property_type = 'AND metaPropertyType.meta_key = "property_type" AND metaPropertyType.meta_value = "commercial"';
                $sales = 'AND metaSales.meta_key = "price" AND metaSales.meta_value > 0';
            } elseif ($params['property_type'] == 'sold') {
                $property_status = 'AND metaPropertyStatus.meta_key = "property_status" AND metaPropertyStatus.meta_value = "sold"';
                $property_type = '';
            } elseif ($params['property_type'] == 'leased') {
                $property_status = 'AND metaPropertyStatus.meta_key = "property_status" AND metaPropertyStatus.meta_value = "leased"';
                $property_type = '';
            }

            if (!empty($params['bedrooms'])) {
                $bedrooms = 'AND metaBedrooms.meta_key = "bedrooms" AND metaBedrooms.meta_value >= ' . $params['bedrooms'];
            }

            if (!empty($params['bathrooms'])) {
                $bathrooms = 'AND metaBathrooms.meta_key = "bedrooms" AND metaBathrooms.meta_value >= ' . $params['bathrooms'];
            }

            if (!empty($params['car_spaces'])) {
                $car_spaces = 'AND metaCarSpaces.meta_key = "bedrooms" AND metaCarSpaces.meta_value >= ' . $params['car_spaces'];
            }

            if (!empty($params['price_min']) || !empty($params['price_max'])) {
                if (in_array($params['property_type'], ['residential_sale', 'commercial_sale'])) {
                    $price = 'AND metaPrice.meta_key = "price"';

                    if (!empty($params['price_min'])) {
                        $price .= ' AND metaPrice.meta_value >= ' . $params['price_min'];
                    }

                    if (!empty($params['price_max'])) {
                        $price .= ' AND metaPrice.meta_value <= ' . $params['price_max'];
                    }
                }

                if (in_array($params['property_type'], ['residential_rent', 'commercial_lease'])) {
                    $price = 'AND metaPrice.meta_key = "rent_amount"';

                    if (!empty($params['price_min'])) {
                        $price .= ' AND metaPrice.meta_value >= ' . $params['price_min'];
                    }

                    if (!empty($params['price_max'])) {
                        $price .= ' AND metaPrice.meta_value <= ' . $params['price_max'];
                    }
                }
            }


            if (!empty($params['category'])) {
                $c = '';

                foreach (explode(',', $params['category']) as $item) {
                    if ($item == 'land') {
                        $land = true;
                    } elseif ($item == 'unit_apartment') {
                        $c[] = 'unit';
                        $c[] = 'apartment';
                    } elseif ($item == 'acreage_rural') {
                        $c[] = 'acreage';
                        $c[] = 'rural';
                    } elseif ($item == 'industrial_warehouse') {
                        $c[] = 'industrial/warehouse';
                    } elseif ($item == 'medical_consulting') {
                        $c[] = 'medical/consulting';
                    } else {
                        $c[] = $item;
                    }
                }

                if (!$land && $property_type == 'residential_sale') {
                    $property_type = 'AND metaPropertyType.meta_key = "property_type" AND metaPropertyType.meta_value = "residential_sale"';
                } elseif ($land && !$c) {
                    $property_type = 'AND metaPropertyType.meta_key = "property_type" AND metaPropertyType.meta_value = "land"';
                }


                if ($c) {
                    $c = implode('","', $c);

                    if ($land) {
                        $property_type = 'AND ((metaPropertyType.meta_key = "property_type" AND metaPropertyType.meta_value = "land") OR ( metaPropertyType.meta_key = "property_type" AND metaPropertyType.meta_value = "residential" AND metaCategory.meta_key = "category" AND metaCategory.meta_value IN ("' . $c . '")))';
                    } else {
                        $category = 'AND metaCategory.meta_key = "category" AND metaCategory.meta_value IN ("' . $c . '")';
                    }
                }
            }

            if (!empty($_GET['sales']) || !empty($_GET['rental'])) {
                $inspection = 'AND ((metaInspectionTimes.meta_key = "inspection_times_0_timestamp_end" AND metaInspectionTimes.meta_value > "' . time() . '")
                    OR (metaInspectionTimes.meta_key = "inspection_times_1_timestamp_end" AND metaInspectionTimes.meta_value > "' . time() . '")
                    OR (metaInspectionTimes.meta_key = "inspection_times_2_timestamp_end" AND metaInspectionTimes.meta_value > "' . time() . '")
                    OR (metaInspectionTimes.meta_key = "inspection_times_3_timestamp_end" AND metaInspectionTimes.meta_value > "' . time() . '")
                    OR (metaInspectionTimes.meta_key = "inspection_times_4_timestamp_end" AND metaInspectionTimes.meta_value > "' . time() . '")
                    OR (metaInspectionTimes.meta_key = "inspection_times_5_timestamp_end" AND metaInspectionTimes.meta_value > "' . time() . '"))';
                $property_type = '';

                if (!empty($_GET['sales'])) {
                    $price = 'AND metaPrice.meta_key = "price"';
                    $price .= ' AND metaPrice.meta_value > 0';
                }

                if (!empty($_GET['rental'])) {
                    $price = 'AND metaPrice.meta_key = "rent_amount"';
                    $price .= ' AND metaPrice.meta_value > 0';
                }
            }

            $query = 'SELECT metaSuburb.meta_value as suburb, metaPostcode.meta_value as postcode, COUNT(DISTINCT(wp_posts.ID)) as count
                    FROM wp_posts
                    INNER JOIN wp_postmeta metaPropertyStatus ON (wp_posts.ID = metaPropertyStatus.post_id)
                    INNER JOIN wp_postmeta metaPropertyType ON (wp_posts.ID = metaPropertyType.post_id)
                    INNER JOIN wp_postmeta metaSuburb ON (wp_posts.ID = metaSuburb.post_id)
                    INNER JOIN wp_postmeta metaPostcode ON (wp_posts.ID = metaPostcode.post_id)
                    ' . ($category || ($land && !$category) ? 'INNER JOIN wp_postmeta metaCategory ON (wp_posts.ID = metaCategory.post_id)' : '') . '
                    ' . ($rental ? 'INNER JOIN wp_postmeta metaRental ON (wp_posts.ID = metaRental.post_id)' : '') . '
                    ' . ($sales ? 'INNER JOIN wp_postmeta metaSales ON (wp_posts.ID = metaSales.post_id)' : '') . '
                    ' . ($bedrooms ? 'INNER JOIN wp_postmeta metaBedrooms ON (wp_posts.ID = metaBedrooms.post_id)' : '') . '
                    ' . ($bathrooms ? 'INNER JOIN wp_postmeta metaBathrooms ON (wp_posts.ID = metaBathrooms.post_id)' : '') . '
                    ' . ($car_spaces ? 'INNER JOIN wp_postmeta metaCarSpaces ON (wp_posts.ID = metaCarSpaces.post_id)' : '') . '
                    ' . ($price ? 'INNER JOIN wp_postmeta metaPrice ON (wp_posts.ID = metaPrice.post_id)' : '') . '
                    ' . ($inspection ? 'INNER JOIN wp_postmeta metaInspectionTimes ON (wp_posts.ID = metaInspectionTimes.post_id)' : '') . '
                    WHERE post_status != "trash"
                    AND wp_posts.post_type = "property"
                    ' . $property_status . '
                    ' . $property_type . '
                    ' . $category . '
                    ' . $rental . '
                    ' . $sales . '
                    ' . $bedrooms . '
                    ' . $bathrooms . '
                    ' . $car_spaces . '
                    ' . $price . '
                    ' . $inspection . '
                    AND metaSuburb.meta_key = "address_suburb"
                    AND metaPostcode.meta_key = "address_postcode"
                    GROUP BY metaSuburb.meta_value';
            return ['data' => $wpdb->get_results($query)];

        } catch (Exception $e) {
            return [
                'code'    => $e->getCode(),
                'message' => $e->getMessage()
            ];
        }
    }

    public function sync()
    {
        try {
            $response = json_decode($this->getProperties());
            if ($response->meta->numRecords) {
                foreach ($response->data as $key => $property) {

                    // Don't add property if suburb does not exist
                    if (empty($property->address_suburb)) {
                        continue;
                    }

                    $this->createOrUpdateProperty($property);
                }

                if(empty($_GET['recent'])) {
                    update_option('property_sync_offset', intval($this->offset + $this->limit));
                }
            } else {
                if(empty($_GET['recent'])) {
                    update_option('property_sync_offset', 0);
                }
            }
            return [
                'offset'             => $this->offset,
                'limit'              => $this->limit,
                'properties_updated' => $this->properties_updated,
                'properties_created' => $this->properties_created,
                'agents_updated'     => $this->agents_updated,
                'agents_created'     => $this->agents_created,
                'suburbs_created'    => $this->suburbs_created
            ];
        } catch (Exception $e) {
            return [
                'code' => $e->getCode(),
                'message' => $e->getMessage()
            ];
        }
    }

    private function getProperties()
    {
        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => $_ENV['DPG_API_URL']
        ]);

        $query = [
            'api_token' => $_ENV['DPG_API_KEY'],
            'where' => 'agency_id,12',
            'offset' => empty($_GET['recent']) ? $this->offset : 0,
            'limit' => empty($_GET['recent']) ? $this->limit : 200
        ];

        if(!empty($_GET['recent'])) {
            $query['orderBy'] = 'updated_at,desc';
        } else {
            $query['orderBy'] = 'created_at,desc';
        }

        $response = $client->request('GET', 'properties', [
            'query' => $query
        ]);

        return $response->getBody()->getContents();
    }

    private function createOrUpdateProperty($property)
    {
        $title = '';

        if ($property->address_street_number) {
            $title = trim(implode(' ', [(!empty($property->address_sub_number) ? $property->address_sub_number . '/' : '') . $property->address_street_number, $property->address_street . ',', $property->address_suburb, $property->address_postcode]), '-');
        }

        // Find existing post
        $found_post = new WP_Query([
            'posts_per_page' => 2,
            'post_type' => 'property',
            'meta_key' => 'id',
            'meta_value' => $property->property_unique_id,
            'post_status' => ['draft', 'pending', 'future', 'publish', 'private']
        ]);

        $force_update = $found_post->found_posts && !empty(get_field('field_579d956ac7a19', $found_post->posts[0]->ID)) ? true : false;

        // Continue if updated_at is changed from api
        if ($found_post->found_posts && $property->updated_at == get_field('field_575d361a1e1ed', $found_post->posts[0]->ID) && !$force_update) {
            return;
        }


        // Create post object
        $post = [
            'post_name' => $property->rea_listing_unique_id,
            'post_title' => $title,
            'post_content' => $property->description,
            'post_type' => 'property',
            'post_author' => 1,
            'post_status' => 'publish',
            'post_date'         => $property->created_at,
            'post_date_gmt'     => get_gmt_from_date($property->created_at),
            'post_modified'     => $property->updated_at,
            'post_modified_gmt' => get_gmt_from_date($property->updated_at),
        ];

        // If existing post found, set id
        if ($found_post->found_posts) {
            $post['ID'] = $found_post->posts[0]->ID;
            $this->properties_updated++;
        } else {
            $this->properties_created++;
        }

        // Create/Update post
        $post_id = wp_insert_post($post);

        // Update ACF fields
        update_field('field_575d06c631d44', $property->property_unique_id, $post_id); // id
        update_field('field_57a57e53c15c3', $property->rea_agency_id, $post_id); // branch_id
        update_field('field_579d956ac7a19', 0, $post_id); // force_update
        update_field('field_579451188301d', $property->headline, $post_id); // headline
        update_field('field_5738021c707f5', $property->property_type, $post_id); // property_type
        update_field('field_575d1ab083c33', $property->property_status, $post_id); // property_status
        update_field('field_575d213ee4873', $property->video_link, $post_id); // video
        update_field('field_5794465801b89', $property->address_full, $post_id); // address_full
        update_field('field_57c0f2f35e551', $property->address_sub_number, $post_id); // address_sub_number
        update_field('field_574138d878576', $property->address_street_number, $post_id); // address_street_number
        update_field('field_575d2389b3b52', $property->address_street, $post_id); // address_street
        update_field('field_575d23a6b3b53', ucwords(strtolower($property->address_suburb)), $post_id); // address_suburb
        update_field('field_575d23e4f01f8', $property->address_state, $post_id); // address_state
        update_field('field_575d2430f01f9', $property->address_postcode, $post_id); // address_postcode
        update_field('field_575d293c23468', $property->price, $post_id); // price
        update_field('field_579d9503c7a18', $property->price_display, $post_id); // price_display
        update_field('field_579d94eec7a17', $property->price_view, $post_id); // price_view
        update_field('field_57d51b1334e73', $property->under_offer, $post_id); // under_offer
        update_field('field_579444616bd70', $property->lat, $post_id); // lat
        update_field('field_579444076bd6f', $property->lon, $post_id); // lon
        update_field('field_575d2a4020ce6', $property->auction_date, $post_id); // auction_date
        update_field('field_575d36111e1ec', $property->created_at, $post_id); // created_at
        update_field('field_575d361a1e1ed', $property->updated_at, $post_id); // updated_at

        // Property Media
        if ( ! empty( $property->property_media ) ) {
            update_field('property_media', $property->property_media[0]->url, $post_id);
        }

        // Property category
        if (!empty($property->category)) {
            update_field('field_575d19a84d6da', $property->category, $post_id);
        } elseif (!empty($property->property_commercial_categories)) {
            update_field('field_575d19a84d6da', $property->property_commercial_categories[0]->commercial_category, $post_id);
        }

        // Property objects
        if (!empty($property->property_objects)) {
            foreach ($property->property_objects as $p) {
                if ($p->object_type == 'floorplan') {
                    update_field('field_579447a323d1d', $p->object_url ?: '', $post_id); // floor_plan
                }
            }
        }

        // Rent details
        if (!empty($property->property_rents[0])) {
            update_field('field_57944cb525333', $property->property_rents[0]->rent_amount ?: '', $post_id); // rent_amount
            update_field('field_57944cc125334', $property->property_rents[0]->rent_period ?: '', $post_id); // rent_period
        } elseif (!empty($property->commercial_rent)) {
            update_field('field_57944cb525333', $property->commercial_rent ?: '', $post_id); // rent_amount
            update_field('field_57944cc125334', $property->commercial_rent_period ?: '', $post_id); // rent_period
        }

        // Sold details
        if (!empty($property->property_sold_details[0])) {
            update_field('field_57944f1525335', $property->property_sold_details[0]->sold_date ?: '', $post_id); // sold_date
            update_field('field_57c129e7b76d2', $property->property_sold_details[0]->sold_price ?: '', $post_id); // sold_price
            update_field('field_57c12e26b76d3', $property->property_sold_details[0]->display_sold_price ?: '', $post_id); // display_sold_price
        }

        // Property inspection times
        if (!empty($property->property_inspection_times)) {
            $times = [];
            foreach ($property->property_inspection_times as $time) {
                $t = explode(' ', $time->inspection_time);

                $times[] = [
                    'label' => $time->inspection_time,
                    'timestamp_start' => strtotime($t[0] . ' ' . $t[1]),
                    'timestamp_end' => strtotime($t[0] . ' ' . $t[3])
                ];
            }

            update_field('field_573809506125a', $times, $post_id);
        }

        // Property features
        $car_spaces = 0;
        foreach ($property->property_features as $feature) {
            $field_id = '';

            switch ($feature->feature_name) {
                case 'Bedrooms':
                    $field_id = 'field_576a828cf7be8';
                    break;
                case 'Bathrooms':
                    $field_id = 'field_576a82a7f7be9';
                    break;
                case 'Garages':
                    $field_id = 'field_576a82b6f7bea';
                    $car_spaces += $feature->feature_value;
                    break;
                case 'Carports':
                    $field_id = 'field_576a82ddf7beb';
                    $car_spaces += $feature->feature_value;
                    break;
                case 'Open Spaces':
                    $field_id = 'field_576a82e9f7bec';
                    $car_spaces += $feature->feature_value;
                    break;
                case 'Living Areas':
                    $field_id = 'field_576a8311f7bed';
                    break;
                case 'Study':
                    $field_id = 'field_576a8322f7bee';
                    break;
                case 'Workshop':
                    $field_id = 'field_576a8330f7bef';
                    break;
                case 'Pay T V':
                    $field_id = 'field_576a8338f7bf0';
                    break;
                case 'Pool Above Ground':
                    $field_id = 'field_576a835cf7bf1';
                    break;
                case 'Ducted Heating':
                    $field_id = 'field_576a8361f7bf2';
                    break;
                case 'Air Conditioning':
                    $field_id = 'field_576a8375f7bf3';
                    break;
                case 'Floorboards':
                    $field_id = 'field_576a8380f7bf4';
                    break;
                case 'Tennis Court':
                    $field_id = 'field_576a838ff7bf5';
                    break;
                case 'Built In Robes':
                    $field_id = 'field_576a839af7bf6';
                    break;
                case 'Pool In Ground':
                    $field_id = 'field_576a83a6f7bf7';
                    break;
                case 'Open Fire Place':
                    $field_id = 'field_576a83abf7bf8';
                    break;
                case 'Intercom':
                    $field_id = 'field_576a83b8f7bf9';
                    break;
                case 'Ducted Cooling':
                    $field_id = 'field_576a83caf7bfa';
                    break;
                case 'Dishwasher':
                    $field_id = 'field_576a83d5f7bfb';
                    break;
                case 'Inside Spa':
                    $field_id = 'field_576a83e0f7bfc';
                    break;
                case 'Outside Spa':
                    $field_id = 'field_576a83e5f7bfd';
                    break;
            }

            update_field($field_id, $feature->feature_value, $post_id);
        }

        // Calculate car spaces (car_spaces + carports + garages)
        update_field('field_57a55d0ab8431', $car_spaces, $post_id);

        // property_images
        if (!empty($property->property_images)) {
            $images = [];
            foreach ($property->property_images as $image) {
                $images[] = ['url' => $image->url];
            }
            update_field('field_575d2d9a15839', $images, $post_id);
        }

        // Add suburb if it does not exist
        if (!empty($property->address_suburb) && !empty($property->address_state && $property->address_postcode)) {
            $found_suburb = get_posts(array(
                'numberposts' => 1,
                'post_type' => 'neighbourhood',
                'title' => ucwords(strtolower($property->address_suburb)),
                'post_status' => ['draft', 'pending', 'future', 'publish', 'private']
            ));

            if (!$found_suburb || $force_update) {
                // Create post object
                $suburb = [
                    'post_title' => ucwords(strtolower($property->address_suburb)),
                    'post_type' => 'neighbourhood',
                    'post_content' => $found_suburb[0]->post_content,
                    'post_author' => 1
                ];

                if (!$found_suburb) {
                    $this->suburbs_created++;
                    $suburb['post_status'] = 'publish';
                } else {
                    $suburb['ID'] = $found_suburb[0]->ID;
                    $suburb['post_status'] = $found_suburb[0]->post_status;
                }

                $suburb_id = wp_insert_post($suburb);

                // Update ACF fields
                update_field('field_57594f572f357', $property->address_postcode, $suburb_id); // postcode
                update_field('field_57594ff02f359', $property->address_state, $suburb_id); // state
            }
        }

        // Property agents
        if ($property->property_agents) {
            $property_agents = [];

            foreach ($property->property_agents as $property_agent) {
                $title = ucwords($property_agent->agent->name);

                $found_agent = get_posts(array(
                    'numberposts' => 1,
                    'post_type' => 'agent',
                    'post_status' => ['draft', 'pending', 'future', 'publish', 'private'],
                    'title' => $title
                ));

                // Create post object
                $agent = [
                    'post_title' => $title,
                    'post_type' => 'agent',
                    'post_author' => 1
                ];

                // If existing post found, set id
                if ($found_agent) {
                    $agent['ID'] = $found_agent[0]->ID;
                    $this->agents_updated++;
                    $agent['post_status'] = $found_agent[0]->post_status;
                    $agent['post_content'] = $found_agent[0]->post_content;
                } else {
                    $this->agents_created++;
                    $agent['post_status'] = 'publish';
                }

                // Create/Update agent
                $agent_post_id = wp_insert_post($agent);

                $property_agents[] = $agent_post_id;

                // Update ACF fields
                update_field('field_576f495abea01', $property_agent->agent->id, $agent_post_id); // agent_id
                update_field('field_57443b884937b', $property_agent->agent->mobile, $agent_post_id); // mobile
                update_field('field_57443bae4937c', $property_agent->agent->phone, $agent_post_id); // phone
                update_field('field_576f48c7c6c04', $property_agent->agent->created_at, $agent_post_id); // created_at
                update_field('field_576f48cfc6c05', $property_agent->agent->updated_at, $agent_post_id); // updated_at

                // Email
                if (!empty($property_agent->agent->email_aliases[0]->email) && strpos($property_agent->agent->email_aliases[0]->email, 'digitalpropertygroup.com') === false) {
                    update_field('field_57443b794937a', $property_agent->agent->email_aliases[0]->email, $agent_post_id);
                } else {
                    update_field('field_57443b794937a', $property_agent->agent->email, $agent_post_id);
                }
            }

            // Update the agents attached to the property
            if ($property_agents) {
                update_field('field_573803c4d1ee0', $property_agents, $post_id); // agents
            }
        }

        // Delete duplicates
        if ($found_post->found_posts >= 2) {
            wp_delete_post($found_post->posts[1]->ID, true);
        }
    }


    public function suburb_demographics()
    {
        try {
            $client = new Client([
                // Base URI is used with relative requests
                'base_uri' => $_ENV['DPG_API_URL']
            ]);

            parse_str($_SERVER['QUERY_STRING'], $params);
            $query = http_build_query(array_merge($params, ['api_token' => $_ENV['DPG_API_KEY']]));

            $response = $client->request('GET', 'suburb-demographics', [
                'query' => $query
            ]);

            return json_decode($response->getBody()->getContents());
        } catch (Exception $e) {
            return [
                'code' => $e->getCode(),
                'message' => $e->getMessage()
            ];
        }
    }

    public function suburb_median_prices()
    {
        try {
            $client = new Client([
                // Base URI is used with relative requests
                'base_uri' => $_ENV['DPG_API_URL']
            ]);

            parse_str($_SERVER['QUERY_STRING'], $params);

            $response = $client->request('GET', 'suburb-median-prices', [
                'query' => array_merge($params, ['api_token' => $_ENV['DPG_API_KEY']])
            ]);

            return json_decode($response->getBody()->getContents());
        } catch (Exception $e) {
            return [
                'code' => $e->getCode(),
                'message' => $e->getMessage()
            ];
        }
    }

    public function weekly_sales_results()
    {
        try {
            parse_str($_SERVER['QUERY_STRING'], $params);
            $client = new Client([
                // Base URI is used with relative requests
                'base_uri' => $_ENV['DPG_API_URL']
            ]);

            $args = [
                'post_type' => 'neighbourhood',
                'orderby' => 'title',
                'order' => 'asc',
                'posts_per_page' => 10,
                'offset' => $params['offset']
            ];

            if ($params['suburb'] != 'any') {
                $args['title'] = $params['suburb'];
            }

            $query = new WP_Query($args);
            $neighbourhoods = [
                'pages' => $query->max_num_pages,
            ];

            foreach ($query->get_posts() as $n) {
                // Get suburb sales results
                $args = [
                    'api_token' => $_ENV['DPG_API_KEY'],
                    'where[0]' => 'suburb,' . $n->post_title,
                    'orderBy' => 'sold_date,desc'
                ];

                $response = $client->request('GET', 'weekly-sales-results', [
                    'query' => $args
                ]);

                $n->auction_results = json_decode($response->getBody()->getContents());
                $neighbourhoods['data'][] = $n;
            }


            return $neighbourhoods;
        } catch (Exception $e) {
            return [
                'code' => $e->getCode(),
                'message' => $e->getMessage()
            ];
        }
    }

    public function weekly_sales_sync()
    {
        try {
            $client = new Client([
                // Base URI is used with relative requests
                'base_uri' => $_ENV['DPG_API_URL']
            ]);

            $query = new WP_Query([
                'post_type' => 'neighbourhood',
                'posts_per_page' => -1,
            ]);

            foreach ($query->get_posts() as $n) {
                // Get suburb sales results
                $args = [
                    'api_token' => $_ENV['DPG_API_KEY'],
                    'where[0]' => 'suburb,' . $n->post_title,
                    'where[1]' => 'state,' . get_field('state', $n->ID),
                    'orderBy' => 'sold_date,desc'
                ];

                $response = $client->request('GET', 'weekly-sales-results', [
                    'query' => $args
                ]);

                $n->auction_results = json_decode($response->getBody()->getContents());
                $neighbourhoods['data'][] = $n;
            }

            return [];
        } catch (Exception $e) {
            return [
                'code' => $e->getCode(),
                'message' => $e->getMessage()
            ];
        }
    }
}
