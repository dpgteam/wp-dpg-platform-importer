<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http://digitalpropertygroup.com
 * @since      1.0.0
 *
 * @package    Wp_DPG_Platform_Importer
 * @subpackage Wp_DPG_Platform_Importer/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Wp_DPG_Platform_Importer
 * @subpackage Wp_DPG_Platform_Importer/includes
 * @author     Paul Beynon <paul@digitalpropertygroup.com>
 */
class Wp_DPG_Platform_Importer_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'wp-dpg-platform-importer',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
