<?php
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
/**
 * Common traits for controller functions.
 *     - Controller initialisation
 *     - API options / url
 */
trait ControllerTrait  {
    protected $base_uri = 'https://platform.digitalpropertygroup.com/api/v1/';
    protected $offset;
    protected $limit = 100;
    protected $options;

    /**
     * Initialises controller so it's ready to make a request.
     * @return void
     */
    protected function initController() {
        $this->offset = get_option('property_sync_offset', 0);
        $this->setOptions();
    }
    /**
     * Gets plugin options from WP DB and stores locally.
     * @return void
     */
    protected function setOptions() {
        $this->options   = get_option( 'wp_dpg_platform_importer_options' );
        $this->api_key   = $this->options['api_key'];
        $this->agency_id = $this->options['agency_id'];
        $this->branch_id = $this->options['branch_id'];
        return;
    }
    /**
     * Gets a new client.
     * @return GuzzleHttp\Client
     */
    protected function client() {
        return new Client([
            'base_uri' => $this->base_uri,
        ]);
    }
    /**
     * Returns a default exception error.
     * @param  Exception $e
     * @return array
     */
    protected function exception(Exception $e) {
        return [
            'code'    => $e->getCode(),
            'message' => $e->getMessage()
        ];
    }
    /**
     * Converts local Australia/Melbourne timestamp to UTC and
     * returns both as array.
     * @param  string $localTimestamp
     * @return array
     */
    protected function getLocalAndUtcTimes($localTimestamp) {
        // $date = Carbon::createFromFormat('Y-m-d H:i:s', '2017-06-30 05:48:57', 'Australia/Melbourne');
        $date = Carbon::createFromFormat('Y-m-d H:i:s', $localTimestamp, 'Australia/Melbourne');
        $timestamps = [];
        $timestamps['local'] = $date->format('Y-m-d H:i:s');
        $date->setTimezone('UTC');
        $timestamps['utc'] = $date->format('Y-m-d H:i:s');
        return $timestamps;
    }
}
