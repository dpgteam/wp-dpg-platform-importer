<?php
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
/**
 * Contains methods for synchronising agent data from DPG Platform.
 */
trait PropertyImportTrait  {
    protected $props = [];
    /**
     * Stores the properties response received during this sync run.
     * @var integer
     */
    protected $property_response    = null;
    /**
     * Stores the number of properties that were created during this sync run.
     * @var integer
     */
    protected $properties_created   = 0;
    /**
     * Stores the number of properties that were updated during this sync run.
     * @var integer
     */
    protected $properties_updated   = 0;
    /**
     * Makes a request for property data from DPG platform and synchronises with local data.
     * @return JSON
     */
    protected function getProperties()
    {
        $client = new Client(['base_uri' => $this->base_uri]);
        $query = [
            'api_token' => $this->api_key,
            'where'     => [
                "agency_id,{$this->agency_id}",
                "has_geolocation_issue,0",
            ],
            'offset'    => empty($_GET['recent']) ? $this->offset : 0,
            'limit'     => empty($_GET['recent']) ? $this->limit : 100,
        ];

        // Query properties by branch id (rea_agency_id)
        if(!empty($this->branch_id)) {
            $query['where'][] = 'rea_agency_id,' . $this->branch_id;
        }

        /* Set query order to created or recently updated. */
        if(!empty($_GET['recent'])) {
            $query['orderBy'] = 'updated_at,desc';
        } else {
            $query['orderBy'] = 'created_at,desc';
        }

        $response = $client->request('GET', 'properties', [
            'query' => $query
        ]);
        $this->property_response = json_decode($response->getBody()->getContents());

        foreach( $this->property_response->data as $property ) {
            /* Non-empty address_full indicates listing, otherwise reset offset. */
            $this->props[] = [
                'address' => $property->address_full,
                'rent' => $property->property_rents[0],
            ];
            if ( strlen($property->address_full) ) {
                $this->createOrUpdateProperty($property);
                $this->updatePropertyNeighbourhood($property, $post_id);
            } else {
                // update_option('property_sync_offset', 0);
            }
        }
        return $this->property_response;
    }

    protected function createOrUpdateProperty($property) {
        if ( ! strlen($property->address_suburb)) {
            return;
        }

        $title = '';
        if ($property->address_street_number) {
            $title = trim(implode(' ', [(!empty($property->address_sub_number) ? $property->address_sub_number . '/' : '') . $property->address_street_number, $property->address_street . ',', $property->address_suburb, $property->address_postcode]), '-');
        }
        /* Find existing post. */
        $found_post = new WP_Query([
            'posts_per_page' => 2,
            'post_type' => 'property',
            'meta_key' => 'id',
            'meta_value' => $property->property_unique_id,
            'post_status' => ['draft', 'pending', 'future', 'publish', 'private']
        ]);

        $force_update = $found_post->found_posts && !empty(get_field('field_579d956ac7a19', $found_post->posts[0]->ID)) ? true : false;

        /* Continue if updated_at is changed from API. */
        // if ($found_post->found_posts && $property->updated_at == get_field('field_575d361a1e1ed', $found_post->posts[0]->ID) && !$force_update) {
            // return;
        // }
        $created = $this->getLocalAndUtcTimes($property->created_at);
        $updated = $this->getLocalAndUtcTimes($property->updated_at);
        /* Create post object. */
        $post = [
            'post_name'         => $property->rea_listing_unique_id,
            'post_title'        => $title,
            'post_content'      => $property->description,
            'post_type'         => 'property',
            'post_author'       => 1,
            'post_status'       => 'publish',
            'post_date'         => $created['local'],
            'post_date_gmt'     => $created['utc'],
            'post_modified'     => $updated['local'],
            'post_modified_gmt' => $updated['utc'],
        ];

        /* If existing post found, set ID. */
        if ($found_post->found_posts) {
            $post['ID'] = $found_post->posts[0]->ID;
            $this->properties_updated++;
        } else {
            $this->properties_created++;
        }
        $post_id = wp_insert_post($post);
        $this->updatePropertyFields($property, $post_id);

        /* Remove any duplicates. */
        if ($found_post->found_posts >= 2) {
            foreach( $found_post->posts as $key => $post ) {
                if( $key > 0 ) {
                    wp_delete_post($found_post->posts[$key]->ID, true);
                }
            }
        }
        return $post_id;
    }
    protected function updatePropertyFields($property, $post_id) {
        /* Standard Fields */
        update_field('field_575d06c631d44', $property->property_unique_id, $post_id); // id
        update_field('field_57a57e53c15c3', $property->rea_agency_id, $post_id); // branch_id
        update_field('field_579d956ac7a19', 0, $post_id); // force_update
        update_field('field_579451188301d', $property->headline, $post_id); // headline
        update_field('field_5738021c707f5', $property->property_type, $post_id); // property_type
        update_field('field_575d1ab083c33', $property->property_status, $post_id); // property_status
        update_field('field_575d213ee4873', $property->video_link, $post_id); // video
        update_field('field_5794465801b89', $property->address_full, $post_id); // address_full
        update_field('field_57c0f2f35e551', $property->address_sub_number, $post_id); // address_sub_number
        update_field('field_574138d878576', $property->address_street_number, $post_id); // address_street_number
        update_field('field_575d2389b3b52', $property->address_street, $post_id); // address_street
        update_field('field_575d23a6b3b53', ucwords(strtolower($property->address_suburb)), $post_id); // address_suburb
        update_field('field_575d23e4f01f8', $property->address_state, $post_id); // address_state
        update_field('field_575d2430f01f9', $property->address_postcode, $post_id); // address_postcode
        update_field('field_579444616bd70', $property->lat, $post_id); // lat
        update_field('field_579444076bd6f', $property->lon, $post_id); // lon
        update_field('field_575d2a4020ce6', $property->auction_date, $post_id); // auction_date
        update_field('field_575d36111e1ec', $property->created_at, $post_id); // created_at
        update_field('field_575d361a1e1ed', $property->updated_at, $post_id); // updated_at

        /* Other Fields */
        $this->updatePropertyCategory($property, $post_id);
        $this->updatePropertyObjects($property, $post_id);
        $this->updatePropertyPricing($property, $post_id);
        $this->updatePropertyInspectionTimes($property, $post_id);
        $this->updatePropertyFeatureFields($property, $post_id);
        $this->updatePropertyImages($property, $post_id);
        $this->updatePropertyAgents($property, $post_id);
    }
    /**
     * Determines the appropriate property catgegory and updates custom field value.
     * @param  object $property
     * @param  integer $post_id
     * @return void
     */
    protected function updatePropertyCategory($property, $post_id) {
        if (!empty($property->category)) {
            update_field('field_575d19a84d6da', $property->category, $post_id);
        } elseif (!empty($property->property_commercial_categories)) {
            update_field('field_575d19a84d6da', $property->property_commercial_categories[0]->commercial_category, $post_id);
        }
    }
    /**
     * Updates floor plan and property media if there is found data.
     * @param  object $property
     * @param  integer $post_id
     * @return void
     */
    protected function updatePropertyObjects($property, $post_id) {
        // Property Media
        if ( ! empty( $property->property_media ) ) {
            update_field('property_media', $property->property_media[0]->url, $post_id);
        }
        // Property objects
        if (!empty($property->property_objects)) :
            foreach ($property->property_objects as $p) :
                if ($p->object_type == 'floorplan') {
                    update_field('field_579447a323d1d', $p->object_url ?: '', $post_id); // floor_plan
                }
            endforeach;
        endif;
    }
    /**
     * Updates property pricing data.
     * @param  Object $property
     * @param  Integer $post_id
     * @return void
     */
    protected function updatePropertyPricing($property, $post_id)
    {
        update_field('field_575d293c23468', $property->price, $post_id); // price
        update_field('field_579d9503c7a18', $property->price_display, $post_id); // price_display
        update_field('field_579d94eec7a17', $property->price_view, $post_id); // price_view
        update_field('field_57d51b1334e73', $property->under_offer, $post_id); // under_offer
        // Rent details
        update_field('field_597146b1c6a35', $property->bond, $post_id); // bond_amount
        update_field('field_59714c71522e5', $property->date_available, $post_id); // date_available
        if (!empty($property->property_rents[0])) {
            update_field('field_57944cb525333', $property->property_rents[0]->rent_amount ?: '', $post_id); // rent_amount
            update_field('field_57944cc125334', $property->property_rents[0]->rent_period ?: '', $post_id); // rent_period
        } elseif (!empty($property->commercial_rent)) {
            update_field('field_57944cb525333', $property->commercial_rent ?: '', $post_id); // rent_amount
            update_field('field_57944cc125334', $property->commercial_rent_period ?: '', $post_id); // rent_period
        }
        // Sold details
        if (!empty($property->property_sold_details[0])) {
            update_field('field_57944f1525335', $property->property_sold_details[0]->sold_date ?: '', $post_id); // sold_date
            update_field('field_57c129e7b76d2', $property->property_sold_details[0]->sold_price ?: '', $post_id); // sold_price
            update_field('field_57c12e26b76d3', $property->property_sold_details[0]->display_sold_price ?: '', $post_id); // display_sold_price
        }
    }
    /**
     * Updates property inspection times.
     * @param  Object $property
     * @param  Integer $post_id
     * @return void
     */
    protected function updatePropertyInspectionTimes($property, $post_id)
    {
        if (!empty($property->property_inspection_times)) {
            $times = [];
            foreach ($property->property_inspection_times as $time) {
                $t = explode(' ', $time->inspection_time);

                $times[] = [
                    'label' => $time->inspection_time,
                    'timestamp_start' => strtotime($t[0] . ' ' . $t[1]),
                    'timestamp_end' => strtotime($t[0] . ' ' . $t[3])
                ];
            }
            update_field('field_573809506125a', $times, $post_id);
        } else {
            update_field('field_573809506125a', [], $post_id);
        }
    }
    /**
     * Extracts property features from $property and saves field values.
     * @param  StdClass $property
     * @param  integer $post_id
     * @return void
     */
    protected function updatePropertyFeatureFields($property, $post_id)
    {
        $car_spaces = 0;
        foreach ($property->property_features as $feature) :
            $field_id = '';
            switch ($feature->feature_name) {
                case 'Bedrooms':
                    $field_id = 'field_576a828cf7be8';
                    break;
                case 'Bathrooms':
                    $field_id = 'field_576a82a7f7be9';
                    break;
                case 'Garages':
                    $field_id = 'field_576a82b6f7bea';
                    $car_spaces += $feature->feature_value;
                    break;
                case 'Carports':
                    $field_id = 'field_576a82ddf7beb';
                    $car_spaces += $feature->feature_value;
                    break;
                case 'Open Spaces':
                    $field_id = 'field_576a82e9f7bec';
                    $car_spaces += $feature->feature_value;
                    break;
                case 'Living Areas':
                    $field_id = 'field_576a8311f7bed';
                    break;
                case 'Study':
                    $field_id = 'field_576a8322f7bee';
                    break;
                case 'Workshop':
                    $field_id = 'field_576a8330f7bef';
                    break;
                case 'Pay T V':
                    $field_id = 'field_576a8338f7bf0';
                    break;
                case 'Pool Above Ground':
                    $field_id = 'field_576a835cf7bf1';
                    break;
                case 'Ducted Heating':
                    $field_id = 'field_576a8361f7bf2';
                    break;
                case 'Air Conditioning':
                    $field_id = 'field_576a8375f7bf3';
                    break;
                case 'Floorboards':
                    $field_id = 'field_576a8380f7bf4';
                    break;
                case 'Tennis Court':
                    $field_id = 'field_576a838ff7bf5';
                    break;
                case 'Built In Robes':
                    $field_id = 'field_576a839af7bf6';
                    break;
                case 'Pool In Ground':
                    $field_id = 'field_576a83a6f7bf7';
                    break;
                case 'Open Fire Place':
                    $field_id = 'field_576a83abf7bf8';
                    break;
                case 'Intercom':
                    $field_id = 'field_576a83b8f7bf9';
                    break;
                case 'Ducted Cooling':
                    $field_id = 'field_576a83caf7bfa';
                    break;
                case 'Dishwasher':
                    $field_id = 'field_576a83d5f7bfb';
                    break;
                case 'Inside Spa':
                    $field_id = 'field_576a83e0f7bfc';
                    break;
                case 'Outside Spa':
                    $field_id = 'field_576a83e5f7bfd';
                    break;
            }
            update_field($field_id, $feature->feature_value, $post_id);
        endforeach;

        // Calculate car spaces (car_spaces + carports + garages)
        update_field('field_57a55d0ab8431', $car_spaces, $post_id);
    }
    /**
     * Updates property images.
     * @param  Object $property
     * @param  Integer $post_id
     * @return void
     */
    protected function updatePropertyImages($property, $post_id)
    {
        if (!empty($property->property_images)) {
            $images = [];
            foreach ($property->property_images as $image) {
                $images[] = ['url' => $image->url];
            }
            update_field('field_575d2d9a15839', $images, $post_id);
        }
    }
}
