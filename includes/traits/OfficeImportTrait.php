<?php
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
/**
 * Contains methods for synchronising an agency's branches via DPG Platform.
 */
trait OfficeImportTrait  {
    /**
     * Stores the number of properties that were created during this sync run.
     * @var integer
     */
    protected $offices_created   = 0;
    protected function getOffices() {
        $client = $this->client();
        $query = [
            'api_token' => $this->api_key,
            'where[]'   => "agency_id,{$this->agency_id}",
        ];
        $response = $client->request('GET', 'branches', [
            'query' => $query,
        ]);
        $branches = json_decode($response->getBody()->getContents())->data;
        foreach($branches as $branch) {
            $found_post = new WP_Query([
                'posts_per_page' => 2,
                'post_type' => 'office',
                'meta_key' => 'branch_id',
                'meta_value' => $branch->rea_agency_branch_id,
                'post_status' => ['draft', 'pending', 'future', 'publish', 'private']
            ]);
            if ( ! count($found_post->posts) ) {
                $title = trim( str_replace($branch->agency->name, '', $branch->name) );
                $post = [
                    'post_title'        => $title,
                    'post_content'      => '',
                    'post_type'         => 'office',
                    'post_author'       => 1,
                    'post_status'       => 'draft',
                ];
                $this->offices_created++;
                $post_id = wp_insert_post($post);
                update_field('field_57a57a39d8761', $branch->rea_agency_branch_id, $post_id);
            }
        }
        return [
            'result'          => 'OK',
            'offices_created' => $this->offices_created,
        ];
    }
}
