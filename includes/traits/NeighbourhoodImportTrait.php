<?php
trait NeighbourhoodImportTrait  {
    private $suburbs_created;
    private $suburbs_updated;
    /**
     * Updates existing $neighbourhood entry or creates a new one.
     * @param  Object $neighbourhood
     * @return void
     */
    protected function updatePropertyNeighbourhood($property) {
        if (!empty($property->address_suburb) && !empty($property->address_state && $property->address_postcode)) {
            $found_suburb = get_posts(array(
                'numberposts'  => 1,
                'post_type'    => 'neighbourhood',
                'title'        => ucwords(strtolower($property->address_suburb)),
                'post_status'  => ['draft', 'pending', 'future', 'publish', 'private']
            ));

            if ( ! $found_suburb || $force_update ) {
                // Create post object
                $suburb = [
                    'post_title' => ucwords(strtolower($property->address_suburb)),
                    'post_type' => 'neighbourhood',
                    'post_content' => $found_suburb[0]->post_content ?? '',
                    'post_author' => 1
                ];

                if (!$found_suburb) {
                    $this->suburbs_created++;
                    $suburb['post_status'] = 'publish';
                } else {
                    $suburb['ID'] = $found_suburb[0]->ID;
                    $suburb['post_status'] = $found_suburb[0]->post_status;
                }

                $post_id = wp_insert_post($suburb);

                // Update ACF fields
                update_field('field_57594f572f357', '$property->address_postcode', $post_id); // postcode
                update_field('field_57594ff02f359', '$property->address_state', $post_id); // state
            }
        }
        return $post_id;
    }
}
