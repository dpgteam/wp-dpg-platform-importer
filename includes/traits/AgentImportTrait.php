<?php
/**
 * Contains methods for synchronising agent data from DPG Platform.
 */
trait AgentImportTrait  {
    /**
     * List of agent names that were created.
     * @var array
     */
    protected $agents_created = [];
    /**
     * List of agent names that were updated.
     * @var array
     */
    protected $agents_updated = [];
	/**
	 * Checks $property data's agents, creates or updates
	 * them in WordPress and updates custom field values.
	 * @param  object $property
	 * @param  integer $post_id
	 * @return void
	 */
    protected function updatePropertyAgents($property, $post_id) {
        // Property agents
        if ($property->property_agents) {
            $property_agents = [];
            foreach ($property->property_agents as $property_agent) {
                $property_agents[] = $this->createOrUpdateAgent($property_agent, $post_id);
            }
            // Update the agents attached to the property
            if ($property_agents) {
                update_field('field_573803c4d1ee0', $property_agents, $post_id); // agents
            }
        }
    }
    /**
     * Gets existing agent from WP database or creates a new
     * one if not found, then updates ACF custom fields.
     * @param  object $property_agent
     * @return integer 		The found agent's post id.
     */
    protected function createOrUpdateAgent($property_agent) {
		$title = ucwords($property_agent->agent->name);

        $found_agent = get_posts( array(
            'numberposts' => 1,
            'post_type'   => 'agent',
            'post_status' => [ 'draft', 'pending', 'future', 'publish', 'private' ],
            'meta_key'    => 'id',
            'meta_value'  => $property_agent->agent->id
        ) );

        // Create post object
        $agent = [
            'post_title' => $title,
            'post_type' => 'agent',
            'post_author' => 1
        ];

        // If existing post found, set id
        if ($found_agent) {
            $this->agents_updated[] = $title;
            $agent['ID'] = $found_agent[0]->ID;
            $agent['post_status'] = $found_agent[0]->post_status;
            $agent['post_content'] = $found_agent[0]->post_content;
        } else {
            $this->agents_created[] = $title;
            $agent['post_status'] = 'publish';
        }
        // Create/Update agent
        $post_id = wp_insert_post($agent);

        // Only update custom fields if the agent has been updated since last synced.
        if ( $property_agent->updated_at !== get_field('updated_at', $post_id) ) {
           $this->updateAgentFields($property_agent, $post_id);
        }
        return $post_id;
    }
    /**
     * Updates ACF custom fields with data from $property_agent
     * @param  object $property_agent
     * @param  integer $post_id
     * @return void
     */
    protected function updateAgentFields($property_agent, $post_id) {
		update_field('field_576f495abea01', $property_agent->agent->id, $post_id); 		   // agent_id
		update_field('field_57443b884937b', $property_agent->agent->mobile, $post_id);     // mobile
		update_field('field_57443bae4937c', $property_agent->agent->phone, $post_id);      // phone
		update_field('field_576f48c7c6c04', $property_agent->agent->created_at, $post_id); // created_at
		update_field('field_576f48cfc6c05', $property_agent->agent->updated_at, $post_id); // updated_at

		if (!empty($property_agent->agent->email_aliases[0]->email) && strpos($property_agent->agent->email_aliases[0]->email, 'digitalpropertygroup.com') === false) {
		    update_field('field_57443b794937a', $property_agent->agent->email_aliases[0]->email, $post_id);
		} else {
		    update_field('field_57443b794937a', $property_agent->agent->email, $post_id);
		}
    }
}
