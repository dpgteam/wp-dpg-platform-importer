<?php
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
/**
 * Contains methods for synchronising sales data from DPG Platform.
 */
trait SuburbDataImportTrait  {
    /**
     * Retrieves suburb population data from the DPG platform.
     * @return array
     */
    public function suburb_demographics()
    {
        try {
            $client = new Client([
                // Base URI is used with relative requests
                'base_uri' => $_ENV['DPG_API_URL']
            ]);

            parse_str($_SERVER['QUERY_STRING'], $params);
            $query = http_build_query(array_merge($params, ['api_token' => $_ENV['DPG_API_KEY']]));

            $response = $client->request('GET', 'suburb-demographics', [
                'query' => $query
            ]);

            return json_decode($response->getBody()->getContents());
        } catch (Exception $e) {
            return [
                'code' => $e->getCode(),
                'message' => $e->getMessage()
            ];
        }
    }
    /**
     * Retrieves suburb median price data from the DPG platform.
     * @return array
     */
    public function suburb_median_prices()
    {
        try {
            $client = new Client([
                // Base URI is used with relative requests
                'base_uri' => $_ENV['DPG_API_URL']
            ]);

            parse_str($_SERVER['QUERY_STRING'], $params);

            $response = $client->request('GET', 'suburb-median-prices', [
                'query' => array_merge($params, ['api_token' => $_ENV['DPG_API_KEY']])
            ]);

            return json_decode($response->getBody()->getContents());
        } catch (Exception $e) {
            return [
                'code' => $e->getCode(),
                'message' => $e->getMessage()
            ];
        }
    }
    /**
     * Retrieves auction results from the DPG Platform for all neighbourhoods
     * or just the suburb passed as query string with request.
     * @return array
     */
    public function weekly_sales_results()
    {
        try {
            parse_str($_SERVER['QUERY_STRING'], $params);
            $client = new Client([
                'base_uri' => $_ENV['DPG_API_URL']
            ]);

            $args = [
                'post_type'      => 'neighbourhood',
                'orderby'        => 'title',
                'order'          => 'asc',
                'posts_per_page' => 10,
                'offset'         => $params['offset']
            ];

            if ($params['suburb'] != 'any') {
                $args['title'] = $params['suburb'];
            }

            $query = new WP_Query($args);
            $neighbourhoods = [
                'pages' => $query->max_num_pages,
            ];

            foreach ($query->get_posts() as $n) {
                // Get suburb sales results
                $args = [
                    'api_token' => $_ENV['DPG_API_KEY'],
                    'where[0]' => 'suburb,' . $n->post_title,
                    'orderBy' => 'sold_date,desc'
                ];

                $response = $client->request('GET', 'weekly-sales-results', [
                    'query' => $args
                ]);

                $n->auction_results = json_decode($response->getBody()->getContents());
                $neighbourhoods['data'][] = $n;
            }
            return $neighbourhoods;
        } catch (Exception $e) {
            return [
                'code' => $e->getCode(),
                'message' => $e->getMessage()
            ];
        }
    }
    /**
     * Seems to duplicate weekly_sales_results() need to check if deprecated.
     * @return array
     */
    public function weekly_sales_sync()
    {
        try {
            $client = new Client([
                // Base URI is used with relative requests
                'base_uri' => $_ENV['DPG_API_URL']
            ]);

            $query = new WP_Query([
                'post_type' => 'neighbourhood',
                'posts_per_page' => -1,
            ]);

            foreach ($query->get_posts() as $n) {
                // Get suburb sales results
                $args = [
                    'api_token' => $_ENV['DPG_API_KEY'],
                    'where[0]' => 'suburb,' . $n->post_title,
                    'where[1]' => 'state,' . get_field('state', $n->ID),
                    'orderBy' => 'sold_date,desc'
                ];

                $response = $client->request('GET', 'weekly-sales-results', [
                    'query' => $args
                ]);

                $n->auction_results = json_decode($response->getBody()->getContents());
                $neighbourhoods['data'][] = $n;
            }
            return [];
        } catch (Exception $e) {
            return [
                'code' => $e->getCode(),
                'message' => $e->getMessage()
            ];
        }
    }

}
