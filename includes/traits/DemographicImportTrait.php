<?php
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
/**
 * Contains methods for synchronising agent data from DPG Platform.
 */
trait DemographicImportTrait  {
    /**
     * Queries the DPG Platform API for suburb population demographics.
     * Accepts a suburb, state or postcode.
     * @todo  Clarify parameter names for documentation.
     *
     * @return json
     */
    public function suburb_demographics()
    {
        try {
            $client = $this->client();
            parse_str($_SERVER['QUERY_STRING'], $params);

            $query = http_build_query(array_merge($params, ['api_token' => $this->api_key]));

            $response = $client->request('GET', 'suburb-demographics', [
                'query' => $query
            ]);

            return json_decode($response->getBody()->getContents());
        } catch (Exception $e) {
            return $this->exception($e);
        }
    }
    /**
     * Queries the DPG Platform API for suburb median price data.
     * Accepts a suburb, state or postcode.
     * @todo  Clarify parameter names for documentation.
     *
     * @return json
     */
    public function suburb_median_prices()
    {
        try {
            $client = $this->client();

            parse_str($_SERVER['QUERY_STRING'], $params);

            $response = $client->request('GET', 'suburb-median-prices', [
                'query' => array_merge($params, ['api_token' => $_ENV['DPG_API_KEY']])
            ]);

            return json_decode($response->getBody()->getContents());
        } catch (Exception $e) {
            return [
                'code' => $e->getCode(),
                'message' => $e->getMessage()
            ];
        }
    }
    /**
     * Queries the DPG Platform API for a suburb's weekly sales data.
     * Accepts a suburb, state or postcode.
     * @todo  Clarify parameter names for documentation.
     *
     * @return json
     */
    public function weekly_sales_results()
    {
        try {
            parse_str($_SERVER['QUERY_STRING'], $params);
            $client = $this->client();
            $args = [
                'post_type' => 'neighbourhood',
                'orderby' => 'title',
                'order' => 'asc',
                'posts_per_page' => 10,
                'offset' => $params['offset']
            ];

            if ($params['suburb'] != 'any') {
                $args['title'] = $params['suburb'];
            }

            $query = new WP_Query($args);
            $neighbourhoods = [
                'pages' => $query->max_num_pages,
            ];
            /* Default request arguments. */
            $args = [
                'api_token' => $_ENV['DPG_API_KEY'],
                'orderBy'   => 'sold_date,desc'
            ];
            foreach ($query->get_posts() as $n) {
                /* Add suburb to request. */
                $response = $client->request('GET', 'weekly-sales-results', [
                    'query' => array_merge($args, ['where[0]'  => 'suburb,' . $n->post_title,])
                ]);

                $n->auction_results = json_decode($response->getBody()->getContents());
                $neighbourhoods['data'][] = $n;
            }


            return $neighbourhoods;
        } catch (Exception $e) {
            return $this->exception($e);
        }
    }

    public function weekly_sales_sync()
    {
        try {
            $client = $this->client();

            $query = new WP_Query([
                'post_type' => 'neighbourhood',
                'posts_per_page' => -1,
            ]);

            foreach ($query->get_posts() as $n) {
                // Get suburb sales results
                $args = [
                    'api_token' => $_ENV['DPG_API_KEY'],
                    'where[0]' => 'suburb,' . $n->post_title,
                    'where[1]' => 'state,' . get_field('state', $n->ID),
                    'orderBy' => 'sold_date,desc'
                ];

                $response = $client->request('GET', 'weekly-sales-results', [
                    'query' => $args
                ]);

                $n->auction_results = json_decode($response->getBody()->getContents());
                $neighbourhoods['data'][] = $n;
            }

            return [];
        } catch (Exception $e) {
            return $this->exception($e);
        }
    }
}
