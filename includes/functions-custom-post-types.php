<?php
function cptui_register_my_cpts() {
    /**
     * Post Type: Offices.
     */
    $labels = array(
        "name"          => __( 'Offices', '' ),
        "singular_name" => __( 'Office', '' ),
    );

    $args = array(
        "label"               => __( 'Offices', '' ),
        "labels"              => $labels,
        "description"         => "",
        "public"              => true,
        "publicly_queryable"  => false,
        "show_ui"             => true,
        "show_in_rest"        => false,
        "rest_base"           => "",
        "has_archive"         => false,
        "show_in_menu"        => true,
        "exclude_from_search" => false,
        "capability_type"     => "post",
        "map_meta_cap"        => true,
        "hierarchical"        => false,
        "rewrite"             => array( "slug" => "office", "with_front" => false ),
        "query_var"           => true,
        "supports"            => array( "title", "editor", "thumbnail" ),
    );

    register_post_type( "office", $args );

    /**
     * Post Type: Agents.
     */

    $labels = array(
        "name"          => __( 'Agents', '' ),
        "singular_name" => __( 'Agent', '' ),
    );

    $args = array(
        "label"               => __( 'Agents', '' ),
        "labels"              => $labels,
        "description"         => "",
        "public"              => true,
        "publicly_queryable"  => false,
        "show_ui"             => true,
        "show_in_rest"        => false,
        "rest_base"           => "",
        "has_archive"         => false,
        "show_in_menu"        => true,
        "exclude_from_search" => false,
        "capability_type"     => "post",
        "map_meta_cap"        => true,
        "hierarchical"        => false,
        "rewrite"             => array( "slug" => "agent", "with_front" => false ),
        "query_var"           => true,
        "supports"            => array( "title", "editor", "thumbnail" ),
    );

    register_post_type( "agent", $args );

    /**
     * Post Type: Properties.
     */

    $labels = array(
        "name"          => __( 'Properties', '' ),
        "singular_name" => __( 'Property', '' ),
    );

    $args = array(
        "label"               => __( 'Properties', '' ),
        "labels"              => $labels,
        "description"         => "",
        "public"              => true,
        "publicly_queryable"  => false,
        "show_ui"             => true,
        "show_in_rest"        => false,
        "rest_base"           => "",
        "has_archive"         => false,
        "show_in_menu"        => true,
        "exclude_from_search" => false,
        "capability_type"     => "post",
        "map_meta_cap"        => true,
        "hierarchical"        => false,
        "rewrite"             => array( "slug" => "property", "with_front" => false ),
        "query_var"           => true,
        "supports"            => array( "title", "editor", "thumbnail" ),
    );

    register_post_type( "property", $args );

    /**
     * Post Type: Neighbourhoods.
     */

    $labels = array(
        "name" => __( 'Neighbourhoods', '' ),
        "singular_name" => __( 'Neighbourhood', '' ),
    );

    $args = array(
        "label" => __( 'Neighbourhoods', '' ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => false,
        "show_ui" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "neighbourhood", "with_front" => false ),
        "query_var" => true,
        "supports" => array( "title", "editor", "thumbnail" ),
    );

    register_post_type( "neighbourhood", $args );
}
add_action( 'init', 'cptui_register_my_cpts' );

function dpg_property_columns_head($defaults) {
    $new = [];
    foreach($defaults as $key => $value) {
        if( $key === 'date') {
            $new['address_suburb'] = 'Suburb';
        }
        $new[$key] = $value;
    }
    return $new;
}
add_filter('manage_property_posts_columns', 'dpg_property_columns_head');

function dpg_property_columns_content($column_name, $post_ID) {
    switch ( $column_name ) {
        case 'address_suburb':
            echo get_field('address_suburb', $post_ID);
            break;
    }
}
add_action('manage_property_posts_custom_column', 'dpg_property_columns_content', 10, 2);

function dpg_property_columns_sortable() {
    $columns['address_suburb'] = 'address_suburb';
    return $columns;
}
add_filter( 'manage_edit-property_sortable_columns', 'dpg_property_columns_sortable' );

function dpg_property_suburb_orderby( $query ) {
    if( ! is_admin() )
        return;
    $orderby = $query->get( 'orderby');
    if( 'address_suburb' == $orderby ) {
        $query->set('meta_key','address_suburb');
    }
}
add_action( 'pre_get_posts', 'dpg_property_suburb_orderby' );
