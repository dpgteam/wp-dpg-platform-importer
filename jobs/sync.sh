#!/bin/bash
wget -O ./output.log http://test-import.dev/api/dpg_platform_importer/sync/
echo "Syncing properties at $(date)" >> "$( dirname "${BASH_SOURCE[0]}" )"/sync.log;
cat ./output.log >> sync.log;
rm ./output.log;
