<?php
require(__DIR__ . '/vendor/autoload.php');
/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://bitbucket.org/dpgteam/wp-dpg-platform-importer
 * @since             0.0.1
 * @package           Wp_DPG_Platform_Importer
 *
 * @wordpress-plugin
 * Plugin Name:       DPG Platform Importer
 * Plugin URI:        https://bitbucket.org/dpgteam/wp-dpg-platform-importer
 * Description:       Queries the DPG Platform API and synchronises property data.
 * Version:           0.0.23
 * Author:            Digital Property Group
 * Author URI:        http://digitalpropertygroup.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wp-dpg-platform-importer
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-wp-dpg-platform-importer-activator.php
 */
function activate_wp_dpg_platform_importer() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wp-dpg-platform-importer-activator.php';
	Wp_DPG_Platform_Importer_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-wp-dpg-platform-importer-deactivator.php
 */
function deactivate_wp_dpg_platform_importer() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wp-dpg-platform-importer-deactivator.php';
	Wp_DPG_Platform_Importer_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_wp_dpg_platform_importer' );
register_deactivation_hook( __FILE__, 'deactivate_wp_dpg_platform_importer' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-wp-dpg-platform-importer.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    0.0.1
 */
function run_wp_dpg_platform_importer() {

	$plugin = new Wp_DPG_Platform_Importer();
	$plugin->run();

}
run_wp_dpg_platform_importer();
