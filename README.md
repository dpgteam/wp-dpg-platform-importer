# WordPress Plugin - DPG Platform Importer
Synchronises real estate data via the DPG Platform API for a real estate agency.
* Creates Properties, Agents, Offices and Neighbourhoods.
* Gets demographic data on suburbs from DPG API.

## Setup
### Instructions for composer.json

* Configure package repository.

* Configure WP and plugin directories.

Example composer.json:
````
{
  "repositories": [
    {
        "type": "vcs",
        "url": "https://bitbucket.org/dpgteam/wp-dpg-platform-importer.git"
    }
  ],
  "require": {
    "dpgteam/wp-dpg-platform-importer": "*",
  },
  "extra": {
    "wordpress-install-dir": "public/wp",
    "installer-paths": {
      "public/app/plugins/{$name}/": [
        "type:wordpress-plugin"
      ],
    }
  }
}
````
### WP Admin Instructions
Activate the plugin via the WordPress Admin menu.
#### Required Plugins
The following plugins need to be installed and activated:

  * Advanced Custom Fields PRO

  * Custom Post Type UI

  * JSON API

#### API Key and Agency ID
In order to access the DPG Platform, the client's API token and their Agency ID need to be stored.

* Click the *DPG Importer* admin menu.

* On the settings page, add the API Token and Agency ID in the provided fields and click save.

#### WP JSON API settings
Activate the **DPG Platform Importer** controller.

* In the admin menu select *Settings > JSON API*.

* Locate the **DPG Platform Importer** in the list and click *Activate*.

### Cron job instructions
Add cron job instructions...
